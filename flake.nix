{
  description = "A Nix-flake-based Bun development environment";

  inputs.nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/0.1.*.tar.gz";

  outputs = { self, nixpkgs }:
    let
      supportedSystems = [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];
      forEachSupportedSystem = f: nixpkgs.lib.genAttrs supportedSystems (system: f {
        pkgs = import nixpkgs { inherit system; config.allowUnfree = true; };
      });
    in
    {
      devShells = forEachSupportedSystem ({ pkgs }: {
        default = pkgs.mkShell {
          packages = with pkgs; [ nodejs yarn jetbrains.webstorm yarn2nix ];
        };
      });

      packages = forEachSupportedSystem ({ pkgs }: {
        default = pkgs.mkYarnPackage {
          name = "shoblog-front";
          src = ./.;

          packageJSON = ./package.json;
          yarnLock = ./yarn.lock;
          yarnNix = ./yarn.nix;
          
          buildPhase = ''
            runHook preBuild;
            mkdir $TEMPDIR/dist
            yarn run build --outDir $TEMPDIR/dist;
            runHook postBuild;
          '';

          installPhase = ''
            runHook preInstall
            mkdir -p $out/
            ls $out 
            cp -r $TEMPDIR/dist/ $out/
            runHook postInstall
          '';

          distPhase = ''exit 0'';

        };
      });
    };
}
